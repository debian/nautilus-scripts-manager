��          �   %   �      @     A  #   _     �  $   �     �     �  9   �     �       �  !  !   �  "   �     �     �       i        �     �     �  /   �  =   �  F   :  	   �  V   �    �  -   �	  9    
     Z
  B   k
     �
     �
  `   �
     *  *   J  �  u  D      7   E     }  %   �     �  �   �  .   p  %   �  "   �  R   �  V   ;  v   �     	  l                	                                                     
                                                            %(name)s, linked as %(links)s %s is not a link, I won't remove it Active All scripts must be installed in %s. Error Error:  Graphic interface not available, please select a command. Link %s removed. Nautilus scripts manager Nautilus scripts manager is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3.

Nautilus scripts manager is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Nautilus scripts manager; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. Nautilus scripts manager web page Please select at most one command. Position Position %s is already taken Script Script %(script_name)s is already linked from %(link)s (use argument -p to add a link in a new position). Script %s is not enabled! Script %s is unkown. Script %s not found. The path %s already exists (and is not a link)! The position %(new_pos)s is already used by script %(owner)s. To make changes effective, you may have to close and restart Nautilus. Warning:  target %(link_target)s of link %(link_path)s is missing or outside %(scripts_folder)s. Project-Id-Version: Nautilus scripts manager
Report-Msgid-Bugs-To: me@pietrobattiston.it
POT-Creation-Date: 2009-11-22 10:24+0100
PO-Revision-Date: 2011-09-27 16:02+0000
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Russian (http://www.transifex.net/projects/p/nautilus-scripts-manager/team/ru/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
 %(name)s, слинкован как %(links)s %s не ссылка, необходимо удалить Активный Скрипты должны устанавливаться в  %s. Ошибка Ошибка: Графический интерфейс недоступен, выберите команду. Ссылка %s удалена. Менеджер скриптов Nautilus Nautilus scripts manager is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3.

Nautilus scripts manager is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Nautilus scripts manager; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. Веб-страница менеджера скриптов Nautilus Выберите хотя бы одну команду. Позиция Позиция %s уже занята Скрипт Скрипт %(script_name)s уже связан с %(link)s (используйте параметр -p для добавления ссылки в новую позицию). Скрипт %s не задействован! Скрипт %s неизвестен. Скрипт %s не найден. Путь %s уже существует (и не является ссылкой)! Позиция %(new_pos)s уже используется скриптом %(owner)s. Для принятия изменений необходимо закрыть и перезапустить Nautilus. Внимание: Цель %(link_target)s ссылки %(link_path)s отсутствует или вне %(scripts_folder)s. 