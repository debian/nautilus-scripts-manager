��          �   %   �      @     A  #   _     �  $   �     �     �  9   �     �       �  !  !   �  "   �     �     �       i        �     �     �  /   �  =   �  F   :  	   �  V   �  �  �  "   m	  =   �	     �	  ,   �	     
     
  M   
     ^
     v
  �  �
      �  )   �  
   �  '   �       �        �     �     �  ;   �  E   +  B   q     �  h   �             	                                                     
                                                            %(name)s, linked as %(links)s %s is not a link, I won't remove it Active All scripts must be installed in %s. Error Error:  Graphic interface not available, please select a command. Link %s removed. Nautilus scripts manager Nautilus scripts manager is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3.

Nautilus scripts manager is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Nautilus scripts manager; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. Nautilus scripts manager web page Please select at most one command. Position Position %s is already taken Script Script %(script_name)s is already linked from %(link)s (use argument -p to add a link in a new position). Script %s is not enabled! Script %s is unkown. Script %s not found. The path %s already exists (and is not a link)! The position %(new_pos)s is already used by script %(owner)s. To make changes effective, you may have to close and restart Nautilus. Warning:  target %(link_target)s of link %(link_path)s is missing or outside %(scripts_folder)s. Project-Id-Version: Nautilus scripts manager
Report-Msgid-Bugs-To: me@pietrobattiston.it
POT-Creation-Date: 2009-11-22 10:24+0100
PO-Revision-Date: 2011-09-27 16:02+0000
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de
Plural-Forms: nplurals=2; plural=(n != 1)
 %(name)s, verknüpft als %(links)s %s ist keine Verknüfung, ich werde es daher nicht entfernen. Aktiv Alle Skripte müssen in %s installiert sein. Fehler Fehler: Das graphische Interface ist nicht verfügbar, bitte einen Befehl auswählen. Link %s wurde entfernt. Nautilus Skript-Manager Nautilus Skript-Manager ist freie Software. Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation herausgegeben, weitergeben und/oder modifizieren, unter Version 3 der Lizenz.

Die Veröffentlichung von Nautilus Skript-Manager erfolgt in der Hoffnung, dass es Ihnen von Nutzen sein wird, aber OHNE JEDE GEWÄHRLEISTUNG - sogar ohne die implizite Gewährleistung der MARKTREIFE oder der EIGNUNG FÜR EINEN BESTIMMTEN ZWECK. Details finden Sie in der GNU General Public License.

Sie sollten eine Kopie der GNU General Public License zusammen mit Nautilus Skript-Manager erhalten haben. Falls nicht, schreiben Sie an die Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. Nautilus Skript-Manager Homepage Bitte mindestens einen Befehl auswählen. Bezeichner Der Bezeichner %s ist bereits vergeben. Skript Skript %(script_name)s ist bereits mit %(link)s verknüpft. Benutzen Sie die Option -p um die Verknüpfung an einer neuen Stelle einzufügen. Script %s ist nicht aktiviert! Script %s ist nicht bekannt. Skript %s nicht gefunden. Der Pfad %s existiert bereits (und ist keine Verknüpfung)! Der Bezeichner %(new_pos)s wird bereits vom Skript %(owner)s benutzt. Um die Änderungen zu übernehmen, starten Sie Nautilus bitte neu. Warnung: Das Ziel %(link_target)s der Verknüfung %(link_path)s fehlt oder ist außerhalb von %(scripts_folder)s. 