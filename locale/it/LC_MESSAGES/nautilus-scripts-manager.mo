��          �   %   �      @     A  #   _     �  $   �     �     �  9   �     �       �  !  !   �  "   �     �     �       i        �     �     �  /   �  =   �  F   :  	   �  V   �  �  �  !   m	  1   �	     �	  0   �	     �	     
  A   
     M
     a
    |
  ,   �  +   )  	   U     _     |  �   �          (     E  6   ]  C   �  Z   �     3  g   @             	                                                     
                                                            %(name)s, linked as %(links)s %s is not a link, I won't remove it Active All scripts must be installed in %s. Error Error:  Graphic interface not available, please select a command. Link %s removed. Nautilus scripts manager Nautilus scripts manager is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3.

Nautilus scripts manager is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Nautilus scripts manager; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. Nautilus scripts manager web page Please select at most one command. Position Position %s is already taken Script Script %(script_name)s is already linked from %(link)s (use argument -p to add a link in a new position). Script %s is not enabled! Script %s is unkown. Script %s not found. The path %s already exists (and is not a link)! The position %(new_pos)s is already used by script %(owner)s. To make changes effective, you may have to close and restart Nautilus. Warning:  target %(link_target)s of link %(link_path)s is missing or outside %(scripts_folder)s. Project-Id-Version: Nautilus scripts manager
Report-Msgid-Bugs-To: me@pietrobattiston.it
POT-Creation-Date: 2009-11-22 10:24+0100
PO-Revision-Date: 2011-09-27 16:02+0000
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=2; plural=(n != 1)
 %(name)s, attivato come %(links)s %s non è un collegamento, quindi non lo rimuovo, Attivato Tutti gli script devono essere installati in %s. Errore Errore:  L'interfaccia grafica non è disponibile, selezionare un comando. Rimosso il link %s. Gestore script di Nautilus Gestore script di Nautilus è software libero; puoi redistribuirlo e/o modificarlo nei termini della GNU General Public License come pubblicato dalla Free Software Foundation, versione 3.

Gestore script di Nautilus è distribuito sperando sia utile, ma SENZA ALCUNA GARANZIA espressa o implicita, di COMMERCIABILITÀ o di IDONEITÀ AD UNO SCOPO PARTICOLARE. Vedere la GNU General Public License per ulteriori dettagli.

Dovresti aver ricevuto una copia della GNU General Public License con Gestore script di Nautilus; se così non fosse, scrivi alla Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. Pagina web del gestore di script di Nautilus Non si può selezionare più di un comando. Posizione La posizione %s è occupata. Script Lo script %(script_name)s è già collegato da %(link)s (utilizza l'argomento -p per aggiungere un collegamento in una nuova posizione). Lo script %s non è attivo! Lo script %s è sconosciuto. Non trovo lo script %s. Il percorso %s esiste già (e non è un collegamento)! La posizione %(new_pos)s è già utilizzata dallo script %(owner)s. Per rendere effettivi i cambiamenti, può essere necessario chiudere e riavviare Nautilus. Attenzione:  il percorso %(link_target)s, a cui punta %(link_path)s, è assente o al di fuori di %(scripts_folder)s. 