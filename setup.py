#!/usr/bin/env python

from os import listdir, path, mkdir
from distutils.core import setup
import sys

dist = setup(name='Nautilus scripts manager',
      version='2.0',
      description='Small app to manage Nautilus scripts',
      license='GPL',
      author='Pietro Battiston',
      author_email='me@pietrobattiston.it',
      url='http://www.pietrobattiston.it/nautilus-scripts-manager',
      scripts=['nautilus-scripts-manager'],
      py_modules=['nautilus_scripts_manager_ui'],
      data_files=[('share/nautilus-scripts-manager/stuff', ['stuff/logo.svg', 'stuff/UI.glade']),
				  ('share/pixmaps', ['stuff/nautilus-scripts-manager.svg']),
				  ('share/applications', ['stuff/nautilus-scripts-manager.desktop'])]+
					[('share/locale/'+lang+'/LC_MESSAGES/', ['locale/'+lang+'/LC_MESSAGES/nautilus-scripts-manager.mo'] ) for lang in listdir('locale')],
      classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: X11 Applications :: GTK',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: GNU General Public License (GPL)',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python',
        'Topic :: Desktop Environment :: Gnome',
        'Topic :: Desktop Environment :: Window Managers :: MetaCity',
        'Topic :: Utilities',
        ]    
     )
